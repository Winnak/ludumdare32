﻿using UnityEngine;
using System.Collections;

public class TempSelector : MonoBehaviour
{
    [SerializeField] private TransformCollection collect;
    [SerializeField] private TransformCollection menuPoints;
    [SerializeField] private AnimationCurve ani;
    [SerializeField] private Transform menu;
    [SerializeField] private Sprite[] otherMenus;
    [SerializeField] private SpriteRenderer otherScreen;

    private int current;
    private IEnumerator coroutine;
    private bool other;

    public int Current
    {
        get { return current; }
        set
        {
            current = value;

            if (collect != null) 
            {
                if (current > collect.transforms.Length - 1)
                    current = 0;
                else if (current < 0)
                    current = collect.transforms.Length - 1;
            }
        }
    }

    private void Awake()
    {
        this.coroutine = MoveTo(Vector3.zero);
    }

    private void Update()
    {
        if (other)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Break) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                StopAllCoroutines();
                StartCoroutine(MoveObjectTo(menu, menuPoints.transforms[0].position));
                this.other = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                Current++;
                if (collect != null) 
                {
                    StopCoroutine(this.coroutine);
                    this.coroutine = MoveTo(collect.transforms[Current].position);
                    StartCoroutine(this.coroutine);
                }
            }

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                Current--;
                if (collect != null) 
                {
                    StopCoroutine(this.coroutine);
                    this.coroutine = MoveTo(collect.transforms[Current].position);
                    StartCoroutine(this.coroutine);
                }
            }

            if (Input.GetKeyDown(KeyCode.Break) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                if (collect != null) 
                {
                    switch (collect.transforms[Current].name)
                    {
                        case "play":
                            StopAllCoroutines();
                            otherScreen.sprite = otherMenus[2];
                            StartCoroutine(MoveObjectTo(menu, menuPoints.transforms[1].position));
                            StartCoroutine(ChangeLevel(0.6f));
                            break;

                        case "instructions":
                            StopAllCoroutines();
                            StartCoroutine(MoveObjectTo(menu, menuPoints.transforms[1].position));
                            otherScreen.sprite = otherMenus[0];
                            this.other = true;
                            break;

                        case "credits":
                            StopAllCoroutines();
                            StartCoroutine(MoveObjectTo(menu, menuPoints.transforms[1].position));
                            otherScreen.sprite = otherMenus[1];
                            this.other = true;
                            break;

                        case "quit":
                            Application.Quit();
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }
    
    IEnumerator MoveTo(Vector3 endPos)
    {
        float start = 0;
        float time = 0.6f;
        
        Vector3 startPos = this.transform.position;
        
        while (start < time)
        {
            start += Time.deltaTime;
            
            this.transform.position = Vector3.Lerp(
                startPos, endPos, this.ani.Evaluate(start / time));
            
            yield return null;
        }
    }
    
    IEnumerator MoveObjectTo(Transform obj, Vector3 endPos)
    {
        float start = 0;
        float time = 0.6f;
        
        Vector3 startPos = obj.position;

        while (start < time)
        {
            start += Time.deltaTime;
            
            obj.position = Vector3.Lerp(startPos, endPos, this.ani.Evaluate(start / time));
            
            yield return null;
        }
    }

    IEnumerator ChangeLevel(float wait)
    {
        yield return new WaitForSeconds(wait);
        Application.LoadLevelAdditive("sword");
        GameObject.Destroy(GameObject.Find("Selector"));
        GameObject.Destroy(GameObject.Find("EventSystem"));
    }
}
