﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Sword2 : MonoBehaviour
{
    private const float SpeedModifier = 7f;

    [SerializeField] private AudioClip[] audioCling;
    [SerializeField] private AudioClip[] audioSwush;
    [SerializeField] private PlayerController player;

    public string sword;

    public bool swinging;

    public Font flunt;

    public string SwordName
    {
        get { return sword; }
        set
        {
            sword = value;
            
            this.ui = this.GetComponent<Text>();
            this.col = GetComponent<BoxCollider2D>();

            this.ui.text = "==[]" + sword + ">";
            this.ui.fontSize = 24;
            this.ui.fontStyle = FontStyle.Bold;
            this.ui.font = flunt;
            
            TextGenerator gen = new TextGenerator();
            var width = gen.GetPreferredWidth(this.ui.text, ui.GetGenerationSettings(Vector2.one));
            var height = gen.GetPreferredHeight(this.ui.text, ui.GetGenerationSettings(Vector2.one));
            this.ui.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            this.ui.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
            
            this.ui.transform.rotation = Quaternion.Euler(0, 0, -this.ui.transform.rotation.eulerAngles.z);
            
            this.col.size = new Vector2(width-60, height/6);
            this.col.offset = new Vector2(this.col.size.x / 2+60, 0);

            this.anim.speed = SpeedModifier / (float)(ui.text.Length + ui.text.Length);
        }
    }

    private Text ui;
    private BoxCollider2D col;

    [SerializeField] public Animator anim;

    private AudioSource audiosource;

    private void Start()
    {
        this.audiosource = GetComponent<AudioSource>();
        this.ui = this.GetComponent<Text>();
        this.col = GetComponent<BoxCollider2D>();
    }
    
    private void Update()
    {
        if (player.dead)
        {
            Destroy(this.transform.parent.gameObject);
        }

        if (Input.GetButtonDown("Joystick " + this.player.playerID + " Button 2") && !swinging)
        {
            Swing();
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player(Clone)" && other.gameObject != player.gameObject)
        {
            other.GetComponent<PlayerController>().HurtSound();
            string temp = other.GetComponent<PlayerController>().sword.SwordName;

            this.player.text.text += char.ToUpper(temp[0]) + temp.Substring(1);
            this.player.Name += char.ToUpper(temp[0]) + temp.Substring(1);

            foreach (var item in NamingManager.dict) 
            {
                if(item.Value.item1 == this.player.Name)
                {
                    item.Value.item1 += char.ToUpper(temp[0]) + temp.Substring(1);
                }
            }

            StartCoroutine(FreezeFrame(1));
            
        }
        else if (other.name == "PlayerSword")
        {
            var opp = other.GetComponent<Sword2>();

            if (opp.swinging && this.swinging) 
            {
                anim.SetTrigger("Cling");
                opp.anim.SetTrigger("Cling");
                audiosource.PlayOneShot(audioCling[Random.Range(0, audioCling.Length)]);
                
                player.Flip();
                player.Cling();
            }
        }
    }

    private void SwushSound()
    {
        audiosource.clip = audioSwush[Random.Range(0, audioSwush.Length)];
        audiosource.Play();
    }

    private void Swing()
    {
        swinging = true;
        anim.SetTrigger("Swing");
    }

    public void Flip()
    {
        /*Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;*/
        
        //this.ui.text = "==[]" + sword + ">";
    }

    IEnumerator FreezeFrame(float time)
    {
        Time.timeScale = 0.1f;
        yield return new WaitForSeconds(time);
        Time.timeScale = 1;
        yield return new WaitForSeconds(time);
        GameObject.Destroy(GameObject.Find("GameManager"));
        Application.LoadLevel("mainmenu");
    }
}






