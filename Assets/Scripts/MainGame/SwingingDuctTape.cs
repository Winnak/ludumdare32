﻿using UnityEngine;
using System.Collections;

public class SwingingDuctTape : MonoBehaviour
{
    private Sword2 ductTape;

    private void Awake()
    {
        this.ductTape = this.GetComponentInChildren<Sword2>();
    }

    public void Unswing()
    {
        this.ductTape.swinging = false;
    }
}
