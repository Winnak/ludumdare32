﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private const float MaxSpeed = 10;
    [SerializeField] private float axis;

    public int playerID;

    private Rigidbody2D rigid;
    private bool dashing;
    public bool facingRight;
    [SerializeField] private int jumps;
    private int dashes;
    private float dashTimer = 0;

    public Sword2 sword;

    private string knightname;
    public string Name
    {
        get {return knightname;}
        set 
        {
            knightname = value;
            text.text = value;
        }
    }

    public UnityEngine.UI.Text text;
    
    [SerializeField] private AudioClip[] audioHit;
    private AudioSource audiosource;
    private Animator animator;
    public bool dead;

    private IEnumerator dashroutine;

    private void Awake()
    {
        this.rigid = this.GetComponent<Rigidbody2D>();
        this.sword = this.GetComponentInChildren<Sword2>();
        this.audiosource = GetComponent<AudioSource>();
        this.animator = GetComponent<Animator>();

        dashroutine = Dash(0);
    }

    private void Start()
    {

    }

    private void Update()
    {
        if (!dead)
        {
            bool grounded = Physics2D.Linecast(rigid.position, 
                                               rigid.position + Vector2.up * -1f, 
                                               1 << LayerMask.NameToLayer("OneWayPlatform") |
                                               1 << LayerMask.NameToLayer("Ground"));

            this.animator.SetBool("Grounded", grounded);

            this.axis = Input.GetAxis("Joystick " + playerID + " Axis X");

            this.animator.SetBool("Moving", this.axis < -0.005f || this.axis > 0.005f);

            if (!sword.swinging)
            {
                if (axis > 0)
                {
                    if (!facingRight) 
                    {
                        Flip();
                    }
                }
                if (axis < 0)
                {
                    if (facingRight) 
                    {
                        Flip();
                    }
                }
            }
            else
            {
                this.rigid.velocity *= 0.85f;
            }

            if (grounded)
            {
                this.jumps = 2;
            }

            if (Input.GetButtonDown("Joystick " + playerID + " Button 0") && jumps > 0 && !dashing)
            {
                this.rigid.velocity = Vector2.up * 18;
                this.dashes = 1;
                jumps--;
            }

            if (Input.GetButtonDown("Joystick " + playerID + " Button 1") && !dashing && dashes > 0 && dashTimer > 0.7f)
            {
                dashTimer = 0;
                dashes--;
                StopCoroutine(dashroutine);
                dashroutine = Dash(0.1f);
                StartCoroutine(dashroutine);
            }

            this.dashTimer += Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        if (!dead)
        {
            if (!dashing)
            {
                this.rigid.velocity = new Vector2(Mathf.Clamp(this.rigid.velocity.x + this.axis, -MaxSpeed, MaxSpeed), this.rigid.velocity.y);
            }

            //Now ignore collisions between the Player and OneWayPlatform layers, 
            //unless you're on the ground or travelling up.
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(("Player" + playerID)),
                                           LayerMask.NameToLayer("OneWayPlatform"), 
                                           this.rigid.velocity.y > 0 || (Input.GetAxis("Joystick " + playerID + " Axis Y") < 0f));
        }
    }
     
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (dashing && other.gameObject.name == "Player(Clone)")
        {
            other.gameObject.GetComponent<PlayerController>().rigid.velocity += this.rigid.velocity * 3;
            this.rigid.velocity = -this.rigid.velocity * 0.4f;
            this.rigid.gravityScale = 3;
            this.rigid.drag = 1;
            this.dashing = false;
            StopCoroutine(dashroutine);
        }
    }

    private void OnCollisionStay2D(Collision2D coll)
    {
        this.dashes = 1;
    }

    public void HurtSound()
    {
        audiosource.clip = audioHit[Random.Range(0, audioHit.Length)];
        audiosource.Play();
        this.dead = true;
        this.animator.SetTrigger("Die");
        this.animator.SetBool("Dead", true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(("Player" + 0)),
                                       LayerMask.NameToLayer(("Player" + 1)), 
                                       true);
        
       /* Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(("Player" + playerID)),
                                       LayerMask.NameToLayer(("Sword")), 
                                       true);*/
    }

    public void Flip()
    {
        // Switch the way the player is labelled as facing
        facingRight = !facingRight;
        
        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;

        this.sword.Flip();
    }

    public void Cling()
    {
        StopCoroutine(dashroutine);
        dashroutine = Dash(1);
        StartCoroutine(dashroutine);
    }

    IEnumerator Dash(float dashTime)
    {
        this.rigid.velocity = Vector2.right * 26 * (facingRight ? 1 : -1);
        this.rigid.gravityScale = 0;
        this.rigid.drag = 0;
        this.dashing = true;
        this.animator.SetBool("Dashing", true);
        this.sword.anim.SetBool("Dashing", true);
        yield return new WaitForSeconds(dashTime);
        this.rigid.gravityScale = 3;
        this.rigid.drag = 1;
        this.dashing = false;
        this.animator.SetBool("Dashing", false);
        this.sword.anim.SetBool("Dashing", true);
    }
}





