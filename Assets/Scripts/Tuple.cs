
public class Tuple<T, U>
{
    public T item1;
    public U item2;

    public Tuple(T value1, U value2)
    {
        this.item1 = value1;
        this.item2 = value2;
    }
}

