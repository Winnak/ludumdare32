using UnityEngine;
using UnityEngine.UI;

public class NamingPlayerTitle : MonoBehaviour
{
    public static Text text;
    
    private void Awake()
    {
        text = this.GetComponent<Text>();

        if (NamingManager.dict.Count == 0)
        {
            for (int i = 0; i < NamingManager.PlayerAmount; i++)
            {
                NamingManager.dict.Add(i, new Tuple<string, string>(string.Empty, string.Empty));
            }
        }
        
        text.text = "Sir(é) Player " + (NamingManager.CurrentPlayerID + 1) + "'s Name";
    }
}

