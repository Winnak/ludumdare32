﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NamingPlayers : MonoBehaviour
{
    [SerializeField] private int nameLength = 10;

    public string PlayerName
    {
        get { return player; }
        set
        {
            player = value;
            text.text = value;
        }
    }
    
    private string player = string.Empty;
    private Text text;
    private bool confirming = false;
    
    private void Reset()
    {
        this.PlayerName = string.Empty;
        NamingPlayerTitle.text.text = "Sir(é) Player " + (NamingManager.CurrentPlayerID + 1) + "'s Name";
        this.confirming = false;
    }
    
    // Use this for initialization
    private void Start()
    {
        this.text = this.GetComponent<Text>();
    }
    
    // Update is called once per frame
    private void Update()
    {
        if (!string.IsNullOrEmpty(NamingManager.dict[NamingManager.CurrentPlayerID].item1))
        {
            NamingManager.CurrentPlayerID++;
        }
        foreach (char c in Input.inputString)
        {
            if (c == "\n" [0] || c == "\r" [0])
            {
                if (confirming && PlayerName.Length <= nameLength) 
                {
                    NamingManager.dict[NamingManager.CurrentPlayerID].item1 = PlayerName;
                    NamingManager.CurrentPlayerID++;
                    this.Reset();
                }
                else 
                {
                    if (PlayerName.Length <= nameLength) 
                    {
                        this.text.text += "\n\nPress Enter again to confirm";
                        confirming = true;
                    }
                }
            }
            else 
            { 
                confirming = false;
                
                if (c == "\b" [0])
                {
                    if (PlayerName != string.Empty)
                    {
                        PlayerName = PlayerName.Substring(0, PlayerName.Length - 1);
                    }
                }
                else if (c == (char)127) 
                {
                    PlayerName = string.Empty;
                }
                else
                {
                    PlayerName += c;
                }
            }
        }
        
        text.color = PlayerName.Length > nameLength ? Color.red : Color.black;
    }
}
