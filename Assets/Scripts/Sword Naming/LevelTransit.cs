﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
#if UNITY_STANDALONE
using NAudio;
using NAudio.Wave;
#endif

public class LevelTransit : MonoBehaviour
{
    [SerializeField] private GameObject playerYellow;
    [SerializeField] private GameObject playerRed;

    // Temp
    [SerializeField] private Color[] colors;

    [SerializeField]
    private Font flunt;

    private Dictionary<int, Tuple<string, string>> playerInfo;
#if UNITY_STANDALONE
    private AudioSource sound;
#endif
    private void Awake()
    {
        this.playerInfo = NamingManager.dict;
        GameObject.DontDestroyOnLoad(this.gameObject);
        Application.LoadLevel("game");
#if UNITY_STANDALONE
        this.sound = GetComponent<AudioSource>();
#endif
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level == 2)
        {
            StartCoroutine(SpawnPlayers());
        }

        string announce = string.Empty;

        for (int i = 0; i < playerInfo.Count; i++)
        {
            if (playerInfo[i].item1 == string.Empty) 
                announce += "nothing";
            else if (playerInfo[i].item1.Length < 20) 
                announce += playerInfo[i].item1;
            else 
                announce += "I am not saying that, sorry ";    

            if (i + 1 < playerInfo.Count) 
            {
                announce += ". versus. ";    
            }
        }

#if UNITY_STANDALONE
        StartCoroutine(Speak(announce));
#endif
    }

    IEnumerator SpawnPlayers()
    {
        yield return new WaitForSeconds(4.5f);

        var canvas = GameObject.Find("Canvas");
        List<Transform> spawnpoints = new List<Transform>(4);
        List<UnityEngine.UI.Text> playernames = new List<UnityEngine.UI.Text>(2);
        
        foreach (var transform in canvas.GetComponentsInChildren<Transform>()) 
        {
            if (transform.name == "SpawnPoint") 
            {
                spawnpoints.Add(transform);
            }
        }

        foreach (var plyname in canvas.GetComponentsInChildren<UnityEngine.UI.Text>())
        {
            playernames.Add(plyname);
        }
        
        for (int i = 0; i < playerInfo.Count; i++)
        {
            var obj = GameObject.Instantiate<GameObject>(i == 0 ? playerYellow : playerRed);
            obj.transform.Translate(spawnpoints[i].position);
            obj.transform.parent = canvas.transform;
            obj.transform.localScale = new Vector3(37f, 37f, 1);
            var pc = obj.GetComponent<PlayerController>();
            pc.playerID = i;
            pc.text = playernames[i];
            pc.sword.flunt = this.flunt;
            pc.sword.SwordName = playerInfo[i].item2;
            pc.Name = playerInfo[i].item1;
            pc.name = "Player(Clone)";
            obj.layer = LayerMask.NameToLayer(("Player" + i));
            
        }
    }

#if UNITY_STANDALONE
    
    IEnumerator Speak(string toSay)
    {
        // Remove the "spaces" in excess
        Regex rgx = new Regex("\\s+");

        // Replace the "spaces" with "% 20" for the link Can be interpreted
        string result = rgx.Replace(toSay, "%20");
        string url = "http://translate.google.com/translate_tts?tl=en&q=" + result;
        WWW www = new WWW(url);

        yield return www;

        using (System.Net.WebClient client = new System.Net.WebClient())
        {
            client.DownloadFile(url, "Temp" + System.IO.Path.DirectorySeparatorChar + "temp.mp3");
        }

        using (Mp3FileReader reader = new Mp3FileReader("Temp" + System.IO.Path.DirectorySeparatorChar + "temp.mp3"))
        {
            WaveFileWriter.CreateWaveFile("Temp" + System.IO.Path.DirectorySeparatorChar + "temp.wav", reader);
        }

        WAV wav = new WAV("Temp" + System.IO.Path.DirectorySeparatorChar + "temp.wav");
        sound.clip = AudioClip.Create("TTS", wav.SampleCount, wav.ChannelCount, wav.Frequency, false);
        sound.clip.SetData(wav.LeftChannel, 0);

        sound.Play();
    }

    private float[] ConvertByteBuffer(byte[] buffer)
    {
        float[] result = new float[buffer.Length];

        for (int i = 0; i < result.Length; i++)
        {
            result[i] = (float)(buffer[i] / 256.0f);
        }
        return result;
    }
#endif
}
