﻿using UnityEngine;
using System.Collections;

public class ActivateNext : MonoBehaviour
{
    [SerializeField] private GameObject next;

    private void OnEnable()
    {
        NamingManager.Next += HandleNext;
    }

    private void OnDisable()
    {
        NamingManager.Next -= HandleNext;
    }

    private void HandleNext ()
    {
        if (next != null)
        {
            next.SetActive(true);
        }

        this.gameObject.SetActive(false);
    }
}
