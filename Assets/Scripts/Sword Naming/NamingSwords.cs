﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NamingSwords : MonoBehaviour
{
    [SerializeField] private int swordLength = 8;

    public string SwordName
    {
        get { return sword; }
        set
        {
            sword = value;

            text.text = "=[]";

            for (int i = 0; i < value.Length; i++) 
            {
                text.text += "■";
            }

            text.text += ">";
        }
    }

    private string sword = string.Empty;
    private Text text;
    private bool confirming = false;

    private void Reset()
    {
        this.SwordName = string.Empty;
        NamingSwordTitle.text.text = "Player " + NamingManager.dict[NamingManager.CurrentPlayerID].item1 + ", Name thou sword";
        this.confirming = false;
    }

    private void Start()
    {
        this.text = this.GetComponent<Text>();
    }
    
    private void Update()
    {
        foreach (char c in Input.inputString)
        {
            if (c == "\n" [0] || c == "\r" [0])
            {
                if (confirming) 
                {
                    NamingManager.dict[NamingManager.CurrentPlayerID].item2 = SwordName;
                    NamingManager.CurrentPlayerID++;
                    Reset();
                }
                else 
                {
                    if (SwordName.Length <= swordLength && SwordName.Length >= 4 ) 
                    {
                        this.text.text += "\n\nPress Enter again to confirm";
                        confirming = true;
                    }
                }
            }
            else 
            { 
                confirming = false;

                if (c == "\b" [0])
                {
                    if (SwordName != string.Empty)
                    {
                        SwordName = SwordName.Substring(0, SwordName.Length - 1);
                    }
                }
                else if (c == (char)127) 
                {
                    SwordName = string.Empty;
                }
                else
                {
                    SwordName += c;
                }
            }
        }

        text.color = SwordName.Length > swordLength || SwordName.Length < 4 ? Color.red : Color.black;
    }
}
