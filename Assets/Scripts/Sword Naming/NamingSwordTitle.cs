﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NamingSwordTitle : MonoBehaviour
{
    public static Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
        text.text = "Player " + NamingManager.dict[NamingManager.CurrentPlayerID].item1 + ", Name thou sword";
    }
}
