
using System.Collections.Generic;

public static class NamingManager
{
    public readonly static int PlayerAmount = 2;
    private static int currentPlayerID = 0;
    
    public static event NextEvent Next;
    public delegate void NextEvent();
    

    /// <summary>
    /// Dictionary:
    ///     int: playerID
    ///     Tuple:
    ///         string: Player name. 
    ///         string: Sword name.
    /// </summary>
    public static Dictionary<int, Tuple<string, string>> dict = new Dictionary<int, Tuple<string, string>>();

    public static int CurrentPlayerID
    {
        get
        {
            return currentPlayerID;
        }
        set
        {
            currentPlayerID = value;

            if (currentPlayerID >= PlayerAmount) 
            {
                currentPlayerID = 0;
                Next();
            }
        }
    }
}

