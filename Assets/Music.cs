﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
    [SerializeField] private AudioClip intro;

    private AudioSource source;

    private void Awake()
    {
        if (GameObject.Find("SoundTrackStarted"))
        {
            Destroy(this.gameObject);
        }

        source = GetComponent<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        this.name = "SoundTrackStarted";
        source.PlayOneShot(intro);
        source.PlayDelayed(2.40f);
    }
}
